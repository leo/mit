import json


FILE_NAME = "mit/state.json"

NEXT_FILE_NAME = "next_file_name"
COMMITS = "commits"
NEXT_COMMIT = "next_commit"


def init():
    state = {}
    with open(FILE_NAME, "w") as state_file:
        state[NEXT_FILE_NAME] = 0
        state[COMMITS] = {}
        json.dump(state, state_file)
        state_file.close()


def get_next_name() -> str:
    with open(FILE_NAME, "rt+") as state_file:
        state = json.load(state_file)
        next = state[NEXT_FILE_NAME]
        state[NEXT_FILE_NAME] = next + 1
        state_file.seek(0)
        json.dump(state, state_file)
        state_file.close()
        return f"commit_{next:04}"


def get_commits() -> {str: str}:
    with open(FILE_NAME, "r") as state_file:
        state = json.load(state_file)
        state_file.close()
        return state[COMMITS]


def save_commits(commits: {str: str}):
    with open(FILE_NAME, "rt+") as state_file:
        state = json.load(state_file)
        state[COMMITS] = commits
        state_file.seek(0)
        json.dump(state, state_file)
        state_file.close()


def get_next_commit() -> str:
    with open(FILE_NAME, "r") as state_file:
        state = json.load(state_file)
        state_file.close()
        return state[NEXT_COMMIT]


def set_next_commit(commit: str):
    with open(FILE_NAME, "rt+") as state_file:
        state = json.load(state_file)
        state[NEXT_COMMIT] = commit
        state_file.seek(0)
        json.dump(state, state_file)
        state_file.close()


def try_read():
    with open(FILE_NAME, "r") as state_file:
        state = json.load(state_file)
        next_commit = state[NEXT_COMMIT]
        commits = state[COMMITS]
        state_file.close()
