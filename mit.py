import os
import sys

from shell import *
import state


def init():
    if check_healthy_mit("./"):
        raise Exception("mit repository already exists here!")
    exec("mkdir mit")
    exec("mkdir mit/commits")
    state.init()
    genesis_commit = state.get_next_name()  # the 0th commit, emtpy
    exec("mkdir mit/commits/" + genesis_commit)
    state.save_commits({genesis_commit: "genesis empty commit"})
    next_commit = state.get_next_name()
    exec("mkdir mit/commits/" + next_commit)
    state.set_next_commit(next_commit)
    exec("find . -mindepth 1 -maxdepth 1 ! -name mit -exec mv -t ./mit/commits/" + next_commit + " {} +")
    exec("mkdir mit/internal_workdir")
    exec("mkdir mitted")
    mount()


def mount():
    commits = sorted(state.get_commits().keys())
    commits = ["mit/commits/" + s for s in commits]
    command =   ("fuse-overlayfs -o " +
                 "lowerdir=" + ":".join(commits) + "," +
                 "upperdir=" + "mit/commits/" + state.get_next_commit() + ","
                 "workdir=mit/internal_workdir" + " "
                 "mitted")
    exec(command)


def umount():
    exec("fusermount -u mitted")


def commit(message: str):
    try:
        umount()
    except Exception:
        pass
    commits = state.get_commits()
    next_commit = state.get_next_commit()
    commits[next_commit] = message
    state.save_commits(commits)
    state.set_next_commit(state.get_next_name())
    exec("mkdir mit/commits/" + state.get_next_commit())
    mount()


def check_healthy_mit(path: str) -> bool:
    # TODO: add more checks
    good = 0
    bad = 0
    if os.path.exists(os.path.join(path, "mit")):
        good += 1
    else:
        bad += 1
    if os.path.exists(os.path.join(path, state.FILE_NAME)):
        good += 1
    else:
        bad += 1
    try:
        state.try_read()
    except Exception:
        bad += 1
    if good != 0 and bad != 0:
        raise Exception("Broken mit repository detected!")
    return bool(good)


def usage():
    print("TODO")


if __name__ == "__main__":
    if not sys.argv[1]:
        usage()
    operation = sys.argv[1]
    if operation == "init":
        init()
    elif operation == "mount":
        mount()
    elif operation == "umount":
        umount()
    elif operation == "commit":
        commit(" ".join(sys.argv[2:]))
    else:
        usage()
