import subprocess


def exec(command: str) -> (str, str):
    """
    :Args:
        command (str): shell command to execute
    :Returns:
        (stdout,stderr): standard-out and standard-error of the command.
    :Raises:
        Exception: If the command returns with non-zero exit code.
    """
    result = subprocess.run(command.split(" "), capture_output=True, text=True)
    if result.returncode != 0:
        raise Exception("return code: " + str(result.returncode) + "\n" + result.stderr)
    return result.stdout, result.stderr
