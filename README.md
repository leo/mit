# MIT

**`git` like mod manager**

*If you do not love git, this is not for you.*

## Idea

A videogame installation usually is a folder with many files and subdirectories.
Installing a mod often requires to add or change a set of files in that directory.
Those files have to be at specific paths with specific names.
Thus, it is not possible to mark a file to be part of a mod.
This gets very confusing fast when several mods are installed.
In order to uninstall or update a mod, it would be necessary to know which files
belong to said mod.

**`git` is good at handling sets of changes to different files in a directory tree.**

So one may come up with using git to manage mods, such that a mod equals one commit.
But git tracks file contents and videogames are often enormous (size wise).
Therefore, operations will soon get either slow or die due to insufficient memory.

But for managing mods we do not need file content changes tracked, it is sufficient
to track files as a whole.
It would be cool to have all files for one mod in a single folder.
This way one can easily access each mod folder and e.g. update it.

**overlayfs** can do this.
Or even better `fuse-overlayfs` so we don't have to be root in order to manage mods.

### Example

```ascii-art
├── mit
│   ├── commits
│   │   ├── commit_0000  # game files
│   │   │   ├── bin
│   │   │   │   ├── game.elf
│   │   │   │   └── resources
│   │   │   ├── resources
│   │   │   │   └── textures
│   │   │   └── settings.txt
│   │   ├── commit_0001  # mod 1
│   │   │   ├── mod_settings.txt
│   │   │   └── resources
│   │   │       └── extra_textures
│   │   └── commit_0002  # directory for the next commit
│   ├── internal_workdir
│   │   └── work
│   └── state.json  # metainformation, like commit message
└── mitted  # merged view, this is where we run the game
    ├── bin
    │   └── game.elf
    ├── mod_settings.txt
    ├── resources
    │   ├── extra_textures
    │   └── textures
    └── settings.txt
```

**mit** uses overlayfs to enable git like managing of mods.

It creates a folder `mit` for all the metadata and `mitted` for the merged view.
(I am very open for ideas how use just a `.mit` folder instead.)

`mitted` the merged view is where we add files, for trying out until the mod works and we want to commit.
We also start the game here.
If we were at `mit/commits/commit_0000` it would run without mods.

## commands

* `mit init`
* `mit mount`
* `mit commit`
* `mit umount`

This is still short, see [TODO](TODO.md).

## Installation

`apt install fuse-overlayfs`

`git clone https://git.fairkom.net/leo/mit/`

And either symlink `mit` to some place in `$PATH`, or add it there.

`echo "export PATH=$(readlink -f mit):$PATH" >> .bashrc`

## TODO

[TODO](TODO.md)